import classs from './styles.scss';

//Add the header to the top of the page
let addHeader = _ => {
    let h1 = document.getElementById("header")
    let h2 = document.createElement('h2');
    h2.innerHTML = "Time flies!";
    document.body.insertBefore(h2, h1);
}

//Get an element by ID
let getElementWithId = (id) => {
    return document.getElementById(id);
}

//Change button on click
let changeOnClick = (element) => {
    element.innerHTML = "Clicked";
    element.classList.remove("clickable");
    element.classList.add("clicked");
}

//The ElementGetter
function ElementGetter(id, classes, element){
    this.id = id;
    this.classes = classes;
    this.element = element;
    this.currentElement = null;
}

//Use the ElementGetter to get 1 or multiple elements by class
ElementGetter.prototype.byClass = function(){
    this.currentElement = document.getElementsByClassName(this.classes);
}

//Log all of the elements
ElementGetter.prototype.logElement = function(){
    if(!(this.currentElement)){
        console.log("There is no elements");
    } else {
        if(this.currentElement.length){
            for(var x of this.currentElement){
                console.log(x);
            }
        } else {
            console.log(this.currentElement);
        }
    }
}

//Bind click function
ElementGetter.prototype.bindClick = function(selector, callbackFunction){
    let element = document.querySelectorAll(selector);
    let that = this;
    element.forEach(function(el){el.addEventListener("click", function(){
            that.currentElement = this;
            changeOnClick(this);
            callbackFunction(that);
        });
    });
}

//The on page load complete function
window.addEventListener('load', function () {
    setTimeout(addHeader, 5000);

    let elementObj = new ElementGetter("", "clickable", "");
    elementObj.byClass();
    elementObj.logElement();
    elementObj.bindClick(".clickable", function(obj){
        obj.logElement();
    });
}, false);


//Example functions for explaining =>
let x = function (){
    let temp = function () {
        if(this == "1"){
            return true;
        } else {
            return false;
        }
    }
}.bind(this);

let y = function (){
    let temp =  () => {
        if(this == "1"){
            return true;
        } else {
            return false;
        }
    }
}